package com.alaric.norris.lib.framework.baseline;

import java.text.SimpleDateFormat;

/**
 * ClassName:      BaseConstants
 * Function:       基础常量
 * Contact:        lingy.sun@htd.cn
 * Author:         AlaricNorris
 * Version:        Ver 1.0
 *
 * @since I used to be a programmer like you, then I took an arrow in the knee
 * ***************************************************************************************************
 * Modified By     AlaricNorris       2019/10/19      15:53
 * Modifications:  Initiation
 * ***************************************************************************************************
 */
public class BaseConstants {


    /**
     * ClassName:      DateTimeFormatter
     * Function:       通用日期时间格式化
     * Contact:        lingy.sun@htd.cn
     * Author:         AlaricNorris
     * Version:        Ver 1.0
     *
     * @since I used to be a programmer like you, then I took an arrow in the knee
     * ***************************************************************************************************
     * Modified By     AlaricNorris       2019/10/19      15:58
     * Modifications:  Initiation
     * ***************************************************************************************************
     */
    public static final class DateTimeFormatter {
        /**
         * 日期时间格式化
         */
        public static final SimpleDateFormat SDF_YYYY_MM_DD_HH$MM$SS = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        public static final SimpleDateFormat SDF_YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd");
        public static final SimpleDateFormat SDF_MM_DD = new SimpleDateFormat("MM-dd");

    }


    public static final class RouterTable {
    }
}
